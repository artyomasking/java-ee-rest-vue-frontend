package rest.entity;

import org.json.JSONObject;

public class Product
{
    private long id;
    private String name;
    private int counts;
    private double price;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }	
    
    

    public int getCounts() {
        return counts;
    }

    public void setCounts(int counts) {
        this.counts = counts;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    
    
    public static void main (String args []) {
        String jsonString = "{\n" +
"\n" +
"  \"name\": \"Hello World\",\n" +
"  \"counts\": 51\n" +
"}" ; //assign your JSON String here
        
        JSONObject obj = new JSONObject(jsonString);
        String name = obj.getString("name");
        Boolean test = obj.has("counts");
        System. out. println ("Hello World " + test);

    }
}