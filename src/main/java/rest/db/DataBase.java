package rest.db;

import rest.entity.Product;
import java.sql.*;
import java.util.ArrayList;

public class DataBase {

    private String url, login, password;
    private Connection con;
    private Statement statement;
    private ResultSet rs;

    public boolean errorDataBase;
    public String infoError;
    private int countRows;

    public DataBase() {
        init();
        connect();

    }

    public void init() {
        url = "jdbc:postgresql://localhost:5432/rest";
        login = "postgres";
        password = "qwerty";
        errorDataBase = false;
        countRows = 0;
    }

    public void connect() {

        try {
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection(url, login, password);
            System.out.println("connect " + url);
            try {
                statement = con.createStatement();
            } catch (Exception e) {
                errorDataBase = true;
                infoError = "Error create statment: " + e;
                System.out.println("Error: " + e);
            }

        } catch (Exception e) {
            errorDataBase = true;
            infoError = "Error connect to data base: " + e;
            System.out.println("Error: " + e);
        }

    }

    public ArrayList<Product> getProductList() {
        ArrayList<Product> products = new ArrayList<Product>();

        ArrayList<String[]> productsString = getProducts();

        for (int i = 0; i < productsString.size(); i++) {
            products.add(new Product());
            products.get(i).setId(Integer.parseInt(productsString.get(i)[0]));
            products.get(i).setName(productsString.get(i)[1]);
            products.get(i).setCounts(Integer.parseInt(productsString.get(i)[2]));
            products.get(i).setPrice(Double.parseDouble(productsString.get(i)[3]));
        }
        return products;

    }

    public ArrayList<String[]> getProducts() {

        ArrayList<String[]> list = new ArrayList<String[]>();
        try {
            String sql = "SELECT \"id\", \"name\", \"counts\", \"price\" FROM \"products\";  ";
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                String[] ls = {rs.getString("id"), rs.getString("name"), rs.getString("counts"), rs.getString("price")};

                list.add(ls);
            }
        } catch (Exception e) {
            System.out.println("e = " + e);
        }

        return list;

    }
    
    public Product getProductByID(int id) {
        
        Product product = new Product();
        String temp = "";
        try {
            String sql = "SELECT \"id\", \"name\", \"counts\", \"price\" FROM \"products\" WHERE id = " + id + ";  ";
            rs = statement.executeQuery(sql);
            rs.next();
            String[] productString  = {rs.getString("id"), rs.getString("name"), rs.getString("counts"), rs.getString("price")};
            product.setId(Integer.parseInt(productString[0]));
            product.setName(productString[1]);
            product.setCounts(Integer.parseInt(productString[2]));
            product.setPrice(Double.parseDouble(productString[3]));
            

        } catch (Exception e) {
            errorDataBase = true;
            infoError = "Error : " + e;
        }
        
        System.out.println("tmp: " + temp);
        return product;
    }

    public int getLastID() {

        int tmp = 0;
        try {
            rs = statement.executeQuery("SELECT id FROM \"products\"  ORDER BY id DESC LIMIT 1;  ");
            rs.next();
            tmp = rs.getInt("id");

        } catch (Exception e) {
            errorDataBase = true;
            infoError = "Error get count rows: " + e;
            System.out.println("Error write data base: " + e);
        }

        System.out.println("last id temp: " + tmp);
        return tmp;
    }

    public int addProduct(String name, int counts, double price) {

        int last_id = getLastID() + 1;

        try {

            String data = "INSERT INTO \"products\" (id, name, counts, price) "
                    + "VALUES (" + last_id + ", \'" + name + "\', " + counts + ", " + price + ");";
            statement.executeUpdate(data);

        } catch (Exception e) {
            errorDataBase = true;
            System.out.println("Error write data base: " + e);
            infoError = "Error write data base: " + e;
        }

        System.out.println("error = " + errorDataBase);

        return last_id;
    }

    public Product updateProductById(Product product) {         
        try { 
         con.setAutoCommit(false);

         statement = con.createStatement();
         String sql = "UPDATE \"products\" " +  
                   "SET name =  '" + product.getName() + "', " + 
                   "counts =  '" + product.getCounts() + "', " + 
                   "price =  '" + product.getPrice() + "' " + 
                   "WHERE id = " + product.getId() + " ;";
         statement.executeUpdate(sql);

         statement.close();
         con.commit(); 
         

        } catch (Exception e) {
            errorDataBase = true;
            System.out.println("Error write data base: " + e);
            infoError = "Error write data base: " + e;
        }
        
        return product;
    }


    private String deleteRequest(int id) {
        return "DELETE FROM \"products\" where id = " + id + ";";
    }

    public void deleteProduct(int id) {

        try {
            String data = deleteRequest(id);
            statement.executeUpdate(data);

        } catch (Exception e) {
            errorDataBase = true;
        }

    }
    
    


}
