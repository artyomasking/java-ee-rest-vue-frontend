/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rest.service;

import java.util.ArrayList;
import java.util.List;
import rest.db.DataBase;
import rest.entity.Product;

/**
 *
 * @author artyo
 */
public class ProductService implements ProductServiceImp {

    DataBase db = new DataBase();

    public void test() {
        System.out.println("test()");
    }

    public ArrayList<Product> getList() {
        ArrayList<Product> products = db.getProductList();;

        return products;
    }

    public Product add(Product productPayload) throws Exception {
        Product product = productPayload;
        try {

            int id = db.addProduct(productPayload.getName(), productPayload.getCounts(), productPayload.getPrice());

            product.setId(Long.valueOf(id));
        } catch (Exception e) {
            throw new Exception("Error while JSON transforming.");
        }

        return product;

    }

    public Product delete(int id) {
        Product product = new Product();

        db.deleteProduct(id);

        return product;
    }

    public Product getById(int id) {
        Product product = new Product();

        product = db.getProductByID(id);

        return product;
    }

    public Product update(Product productPayload) throws Exception {
        Product product = productPayload;
        try {

            product = db.updateProductById(productPayload);

        } catch (Exception e) {
            throw new Exception("Error while JSON transforming.");
        }

        return product;

    }
}
