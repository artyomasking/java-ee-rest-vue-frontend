/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rest.service;

import java.util.ArrayList;
import rest.entity.Product;

/**
 *
 * @author artyo
 */
public interface ProductServiceImp {
    void test();
    ArrayList<Product> getList();
    Product add(Product product) throws Exception ;
    Product delete(int id);
    Product getById(int id);
    Product update(Product product) throws Exception;
    
}
