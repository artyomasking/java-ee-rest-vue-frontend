/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rest.resource;

import java.util.List;
import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import jdk.nashorn.internal.parser.JSONParser;
import org.json.JSONObject;
import rest.db.DataBase;
import rest.entity.Product;
import rest.service.ProductServiceImp;

/**
 *
 * @author artyo
 */
@Path("/product")
public class ProductResource {

    @Inject
    ProductServiceImp service;

    DataBase db = new DataBase();

    @GET
    @Path("/")
    @Consumes("application/json")
    @Produces("application/json")
    public Response getProductList() {
        service.test();
        Jsonb jsonb = JsonbBuilder.create();

        List<Product> products = service.getList();

        String resultJSON = jsonb.toJson(products);

        return Response.ok(resultJSON).build();
    }

    @POST
    @Path("/")
    @Consumes("application/json")
    @Produces("application/json")
    public Response addProduct(String payload) {
        Jsonb jsonb = JsonbBuilder.create();
        Product product;
        String productJSON;
        try {
            product = jsonb.fromJson(payload, Product.class);

            Product newProduct = service.add(product);

            productJSON = jsonb.toJson(newProduct);
        } catch (JsonbException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
        return Response.ok(productJSON).build();
    }

    @GET
    @Path("/{isbn}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response getProductById(@PathParam("isbn") String id) {
        Product product = new Product();

        product = service.getById(Integer.parseInt(id));

        return Response.ok(product).build();
    }

    @PUT
    @Path("/{isbn}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response updateProduct(@PathParam("isbn") String id, String payload) {
     
        Jsonb jsonb = JsonbBuilder.create();
        Product productPayload = new Product();
        productPayload = jsonb.fromJson(payload, Product.class);
        Product product = service.getById(Integer.parseInt(id));
        
        JSONObject jsonPayLoad = new JSONObject(payload);
        
        if(jsonPayLoad.has("name")) {
            product.setName(productPayload.getName());
        }
        if(jsonPayLoad.has("price")) {
            product.setPrice(productPayload.getPrice());
        }
        
        if(jsonPayLoad.has("counts")) {
            product.setCounts(productPayload.getCounts());
        }
        
           try {
               
               
               
               product = service.update(product);
           } catch (Exception e) {
               System.out.println("Error while updating: " + e);
           
           }
        

        return Response.ok(product).build();
    }

    @DELETE
    @Path("/{isbn}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response deleteProduct(@PathParam("isbn") String id) {
        service.delete(Integer.parseInt(id));
        String test = "{\"id\":" + id + " }";
        return Response.ok(test).build();
    }
    
}
